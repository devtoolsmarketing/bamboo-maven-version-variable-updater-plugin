package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.agent.bootstrap.AgentContext;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.task.TaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.task.TaskType;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.remote.RemoteAgent;
import com.atlassian.bamboo.v2.build.agent.remote.sender.BambooAgentMessageSender;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.atlassian.spring.container.ContainerManager;
import org.jetbrains.annotations.NotNull;

public class AdvanceMavenVersionVariableTask implements TaskType
{
    public static final String VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY = "variablePattern";
    public static final String VERSION_PATTERN_TO_MATCH_CONFIG_KEY = "versionPattern";
    public static final String INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY = "includeGlobals";

    private PlanManager planManager;
    private VariableDefinitionManager variableDefinitionManager;
    private BambooAgentMessageSender bambooAgentMessageSender;

    @NotNull
    @java.lang.Override
    public TaskResult execute(@NotNull final TaskContext taskContext) throws TaskException
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final BuildContext parentBuildContext = taskContext.getBuildContext().getParentBuildContext();
        if (parentBuildContext == null)
        {
            return TaskResultBuilder.create(taskContext).success().build();
        }

        final String buildResultKey = taskContext.getBuildContext().getBuildResultKey();
        final String topLevelPlanKey = parentBuildContext.getPlanResultKey().getKey();

        final AgentContext context = RemoteAgent.getContext();

        final String variableNameRegex = taskContext.getConfigurationMap().get(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY);
        final String variableValueRegex = taskContext.getConfigurationMap().get(VERSION_PATTERN_TO_MATCH_CONFIG_KEY);
        final boolean includeGlobals = Boolean.parseBoolean(taskContext.getConfigurationMap().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));

        if (context != null)
        {
            // We're in a remote agent. Things now get alot more complicated because we can't get access to managers
            // we want. Send something back home so they can do what we want instead.
            if (bambooAgentMessageSender == null)
            {
                bambooAgentMessageSender = (BambooAgentMessageSender) ContainerManager.getComponent("bambooAgentMessageSender");
            }

            bambooAgentMessageSender.send(new AdvanceMavenVariablesMessage(
                    buildResultKey,
                    variableNameRegex,
                    variableValueRegex,
                    includeGlobals
            ));
        }
        else
        {
            VariableUpdater updater = new VariableUpdater(planManager, variableDefinitionManager, buildLogger);
            updater.update(topLevelPlanKey, variableNameRegex, variableValueRegex, includeGlobals);
        }


        return TaskResultBuilder.create(taskContext).success().build();
    }

    public void setPlanManager(PlanManager planManager)
    {
        this.planManager = planManager;
    }

    public void setBambooAgentMessageSender(BambooAgentMessageSender bambooAgentMessageSender)
    {
        this.bambooAgentMessageSender = bambooAgentMessageSender;
    }

    public void setVariableDefinitionManager(VariableDefinitionManager variableDefinitionManager)
    {
        this.variableDefinitionManager = variableDefinitionManager;
    }
}