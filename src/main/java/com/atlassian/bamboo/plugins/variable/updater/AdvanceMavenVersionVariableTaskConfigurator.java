package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.regex.Pattern;

import static com.atlassian.bamboo.plugins.variable.updater.AdvanceMavenVersionVariableTask.INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY;
import static com.atlassian.bamboo.plugins.variable.updater.AdvanceMavenVersionVariableTask.VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY;
import static com.atlassian.bamboo.plugins.variable.updater.AdvanceMavenVersionVariableTask.VERSION_PATTERN_TO_MATCH_CONFIG_KEY;

public class AdvanceMavenVersionVariableTaskConfigurator extends AbstractTaskConfigurator
{
    private static final String DEFAULT_VARIABLE_PATTERN = ".*autoupdate";

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY, params.getString(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY));
        config.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, params.getString(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
        config.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, params.getString(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));

        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        context.put(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY, DEFAULT_VARIABLE_PATTERN);
        context.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, "");
        context.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, false);
    }

    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        context.put(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY, taskDefinition.getConfiguration().get(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY));
        context.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, taskDefinition.getConfiguration().get(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
        context.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
    }

    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        context.put(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY, taskDefinition.getConfiguration().get(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY));
        context.put(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, taskDefinition.getConfiguration().get(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
        context.put(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY, taskDefinition.getConfiguration().get(INCLUDE_GLOBAL_VARIABLE_CONFIG_KEY));
    }

    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        final String variablePatternToMatch = params.getString(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY);
        if (StringUtils.isEmpty(variablePatternToMatch))
        {
            errorCollection.addError(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.variable.empty.error"));
        }

        try
        {
            Pattern.compile(variablePatternToMatch);
        }
        catch (Exception e)
        {
            errorCollection.addError(VARIABLE_PATTERN_TO_MATCH_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.variable.invalid.error"));
        }
        if (!StringUtils.isBlank(VERSION_PATTERN_TO_MATCH_CONFIG_KEY))
        {
            try
            {
                Pattern versionPattern = Pattern.compile(params.getString(VERSION_PATTERN_TO_MATCH_CONFIG_KEY));
            }
            catch (Exception e)
            {
                errorCollection.addError(VERSION_PATTERN_TO_MATCH_CONFIG_KEY, getI18nBean().getText("com.atlassian.bamboo.plugins.variable.updater.value.invalid.error"));
            }
        }

    }
}
