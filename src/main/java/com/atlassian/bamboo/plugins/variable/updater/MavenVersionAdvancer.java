package com.atlassian.bamboo.plugins.variable.updater;


import org.apache.maven.shared.release.versions.DefaultVersionInfo;
import org.apache.maven.shared.release.versions.VersionParseException;

public class MavenVersionAdvancer
{
    public static String advance(String version) throws VersionParseException
    {
        final DefaultVersionInfo defaultVersionInfo = new DefaultVersionInfo(version);
        return defaultVersionInfo.getNextVersion().toString();
    }
}
