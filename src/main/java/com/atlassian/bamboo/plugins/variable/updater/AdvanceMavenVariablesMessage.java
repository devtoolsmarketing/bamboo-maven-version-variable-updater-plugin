package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.v2.build.agent.messages.AbstractBambooAgentMessage;
import com.atlassian.bamboo.variable.VariableDefinitionManager;

public class AdvanceMavenVariablesMessage extends AbstractBambooAgentMessage
{
    private final String buildResultKey;
    private final String variableNameRegex;
    private final String variableValueRegex;
    private final boolean includeGlobals;

    public AdvanceMavenVariablesMessage(final String buildResultKey, final String variableNameRegex, final String variableValueRegex, final boolean includeGlobals)
    {
        this.buildResultKey = buildResultKey;
        this.variableNameRegex = variableNameRegex;
        this.variableValueRegex = variableValueRegex;
        this.includeGlobals = includeGlobals;
    }

    @Override
    public Object deliver()
    {
        final PlanManager planManager = getComponent(PlanManager.class, "planManager");
        final VariableDefinitionManager variableDefinitionManager = getComponent(VariableDefinitionManager.class, "variableDefinitionManager");
        final BuildLoggerManager buildLoggerManager = getComponent(BuildLoggerManager.class, "buildLoggerManager");


        PlanResultKey planResultKey = PlanKeys.getPlanResultKey(buildResultKey);

        final BuildLogger buildLogger = buildLoggerManager.getBuildLogger(planResultKey);

        final VariableUpdater updater = new VariableUpdater(planManager, variableDefinitionManager, buildLogger);

        updater.update(PlanKeys.getChainResultKey(planResultKey).getPlanKey().getKey(), variableNameRegex, variableValueRegex, includeGlobals);

        return null;
    }


}
