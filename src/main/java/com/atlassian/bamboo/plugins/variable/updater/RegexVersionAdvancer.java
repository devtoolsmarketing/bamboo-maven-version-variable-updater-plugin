package com.atlassian.bamboo.plugins.variable.updater;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexVersionAdvancer
{
    public static String advance(String versionString, Pattern regex)
    {
        final Matcher matcher = regex.matcher(versionString);
        if (matcher.find() && matcher.groupCount() == 1)
        {
            int start = matcher.start(1);
            int end = matcher.end(1);
            long version = Long.parseLong(matcher.group(1));
            long newVersion = version + 1;

            StringBuilder builder = new StringBuilder();
            builder.append(versionString.substring(0, start));
            builder.append(String.valueOf(newVersion));
            builder.append(versionString.substring(end, versionString.length()));
            return builder.toString();
        }
        return versionString;
    }
}
