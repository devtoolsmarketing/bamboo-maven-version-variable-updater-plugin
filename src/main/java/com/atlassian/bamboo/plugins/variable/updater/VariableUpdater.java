package com.atlassian.bamboo.plugins.variable.updater;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.variable.VariableDefinition;
import com.atlassian.bamboo.variable.VariableDefinitionManager;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.apache.maven.shared.release.versions.VersionParseException;
import org.tuckey.web.filters.urlrewrite.utils.StringUtils;

import javax.annotation.Nullable;
import java.util.List;
import java.util.regex.Pattern;

public class VariableUpdater
{
    private final PlanManager planManager;
    private final VariableDefinitionManager variableDefinitionManager;
    private final BuildLogger buildLogger;

    public VariableUpdater(final PlanManager planManager, final VariableDefinitionManager variableDefinitionManager, final BuildLogger buildLogger)
    {
        this.planManager = planManager;
        this.variableDefinitionManager = variableDefinitionManager;
        this.buildLogger = buildLogger;
    }


    public void update(final String topLevelPlanKey, final String variableNameRegex, final String variableValueRegex, final boolean includeGlobals)
    {
        final List<VariableDefinition> planVariables = getVariablesForPlan(topLevelPlanKey);

        final List<VariableDefinition> variablesToChange = Lists.newArrayList();

        Iterables.addAll(variablesToChange, findMatchingVariables(variableNameRegex, planVariables));
        if (includeGlobals)
        {
            Iterables.addAll(variablesToChange, findMatchingVariables(variableNameRegex, variableDefinitionManager.getGlobalVariables()));
        }

        for (final VariableDefinition variableDefinition : variablesToChange)
        {
            advanceVersion(buildLogger, variableDefinition, variableValueRegex);
        }
    }

    private void advanceVersion(BuildLogger buildLogger, VariableDefinition variableDefinition, String variableValueRegex)
    {
        try
        {
            final String oldValue = variableDefinition.getValue();
            final String newValue = StringUtils.isBlank(variableValueRegex) ?
                    MavenVersionAdvancer.advance(oldValue) : RegexVersionAdvancer.advance(oldValue, Pattern.compile(variableValueRegex));

            if (oldValue != null && !oldValue.equals(newValue))
            {
                variableDefinition.setValue(newValue);
                variableDefinitionManager.saveVariableDefinition(variableDefinition);

                buildLogger.addErrorLogEntry(
                        "Changing variable " + variableDefinition.getKey() +
                                " of type " + variableDefinition.getVariableType().name() +
                                " from " + oldValue +
                                " to " + newValue);
            }
        }
        catch (VersionParseException e)
        {
            buildLogger.addErrorLogEntry("Unable to advance " + variableDefinition.getKey(), e);
        }
    }

    private List<VariableDefinition> getVariablesForPlan(final String planKey)
    {
        final Plan planByKey = planManager.getPlanByKey(PlanKeys.getPlanKey(planKey));
        return variableDefinitionManager.getPlanVariables(planByKey);
    }

    private Iterable<VariableDefinition> findMatchingVariables(final String regexToMatch, final List<VariableDefinition> planVariables)
    {
        final Pattern variablePatternToMatch = Pattern.compile(regexToMatch);
        return Iterables.filter(planVariables, new Predicate<VariableDefinition>()
        {
            @Override
            public boolean apply(@Nullable VariableDefinition variableDefinition)
            {
                return variablePatternToMatch.matcher(variableDefinition.getKey()).matches();
            }
        });
    }


}
