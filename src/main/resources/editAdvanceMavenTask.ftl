[@ww.textfield labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.regex" name="variablePattern" required='true'/]
[@ww.textfield labelKey="com.atlassian.bamboo.plugins.variable.updater.value.regex" name="versionPattern" /]
[@ww.checkbox labelKey="com.atlassian.bamboo.plugins.variable.updater.variable.globals" name="includeGlobals" /]